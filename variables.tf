variable "region" {
  description = "AWS region"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
  default     = ""
}

variable "suffix" {
  description = "Suffix in ECS cluster name"
  type        = string
  default     = ""
}

variable "cluster_name" {
  description = "ECS cluster name"
  type        = string
}

variable "cluster_settings" {
  description = "ECS cluster settings (CloudWatch Container Insights, for example)"
  type        = list(map(string))
  default     = []
}

variable "default_capacity_provider_weight" {
  description = "The relative percentage of the total number of launched tasks that should use the default capacity provider"
  type        = number
  default     = 1
}

variable "default_capacity_provider_base" {
  description = "The number of tasks, at a minimum, to run on the specified capacity provider"
  type        = number
  default     = 0
}

variable "managed_termination_protection" {
  description = "Enables or disables container-aware termination of instances in the auto scaling group when scale-in happens"
  type        = string
  default     = "ENABLED"
}

variable "minimum_scaling_step_size" {
  description = "The minimum step adjustment size. A number between 1 and 10,000"
  type        = number
  default     = 1
}

variable "maximum_scaling_step_size" {
  description = "The maximum step adjustment size. A number between 1 and 10,000"
  type        = number
  default     = 1000
}

variable "managed_scaling_status" {
  description = "Whether auto scaling is managed by ECS. Valid values are ENABLED and DISABLED"
  type        = string
  default     = "ENABLED"
}

variable "managed_scaling_target_capacity" {
  description = "The target utilization for the capacity provider. A number between 1 and 100"
  type        = number
  default     = 100
}

variable "ecs_ami_filter" {
  description = "Filter for AWS ECS AMI"
  type        = list(string)
  default     = ["amzn-ami-*-amazon-ecs-optimized"]
}

variable "vpc_security_group_ids" {
  description = "List of VPC security groups to associate with ECS EC2"
  type        = list(string)
  default     = []
}

variable "subnet_ids" {
  description = "A list of VPC subnet IDs for ECS EC2"
  type        = list(string)
}

variable "ec2_instance_type" {
  description = "ECS EC2 instance type"
  type        = string
}

variable "spot_price" {
  description = "The price to use for reserving spot instances"
  type        = string
  default     = ""
}

variable "vpc_id" {
  description = "The VPC for security group creating"
  type        = string
}

variable "alb_security_group_id" {
  description = "ALB security group ID attached to ECS EC2 security group"
  type        = string
  default     = null
}

variable "ec2_data_volume_size_gb" {
  description = "EBS data volume size"
  type        = number
  default     = 22
}

variable "asg_min_size" {
  description = "The minimum size of the auto scale group of ECS EC2 instances"
  type        = number
  default     = 0
}

variable "asg_max_size" {
  description = "The maximum size of the auto scale group of ECS EC2 instances"
  type        = number
  default     = 10
}

variable "asg_desired_capacity" {
  description = "Desired number of ECS EC2 instances in auto scale group"
  type        = number
  default     = null
}

variable "asg_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out"
  type        = number
  default     = 0
}

variable "ssh_key_pair_name" {
  description = "The SSH key name attached to ECS EC2"
  type        = string
  default     = ""
}

variable "efs_servername" {
  description = "EFS server hostname"
  type        = string
  default     = ""
}

variable "efs_mountpoint" {
  description = "Mountpoint for EFS on ECS EC2"
  type        = string
  default     = "/mnt/efs"
}

variable "preinstall_user_script" {
  description = "The command executed at the beginning of startup user script. For using with any custom purposes"
  type        = string
  default     = ""
}

variable "postinstall_user_script" {
  description = "The command executed at the end of startup user script. For using with any custom purposes"
  type        = string
  default     = ""
}

variable "ecs_secrets_support" {
  description = "Enable support of access to ECS secrets from SSM or Secret Manager"
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
