${preinstall_user_script}%{ if efs_servername != "" }Content-Type: multipart/mixed; boundary="==BOUNDARY=="
MIME-Version: 1.0

--==BOUNDARY==
Content-Type: text/cloud-boothook; charset="us-ascii"

# Install amazon-efs-utils
cloud-init-per once yum_update yum update -y
cloud-init-per once install_amazon-efs-utils yum install -y amazon-efs-utils

# Create /efs folder
cloud-init-per once mkdir_efs mkdir ${efs_mountpoint}

# Mount /efs
cloud-init-per once mount_efs echo -e '${efs_servername}:/ ${efs_mountpoint} efs defaults,_netdev 0 0' >> /etc/fstab
mount -a

--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"
%{ endif }

#!/usr/bin/env bash

echo 'ECS_CLUSTER=${ecs_cluster}' >> /etc/ecs/ecs.config
%{ if ecs_secrets_support }echo 'ECS_ENABLE_AWSLOGS_EXECUTIONROLE_OVERRIDE=true' >> /etc/ecs/ecs.config%{ endif }
${postinstall_user_script}
--==BOUNDARY==--
