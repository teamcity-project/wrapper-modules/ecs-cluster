terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  # aws_ecs_capacity_provider resource supported from v2.42
  required_providers {
    aws = "~> 2.42"
  }
}

provider "aws" {
  region = var.region
}

data "aws_ami" "amazon_linux_ecs" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = var.ecs_ami_filter
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}

# It's workaround until capacity_provider makes mutable (https://github.com/terraform-providers/terraform-provider-aws/issues/11287)
resource "random_id" "cap_prov" {
  byte_length = 8
}


# TODO: move aws_ecs_capacity_provider from wrapper module to separate module
resource "aws_ecs_capacity_provider" "ecs_cap_prov" {
  // capacity provider name must be the same with ASG to prevent deletion issue
  name = module.ecs_asg.this_autoscaling_group_name
  // name = "${var.cluster_name}-asg2"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = module.ecs_asg.this_autoscaling_group_arn
    managed_termination_protection = var.managed_termination_protection

    managed_scaling {
      minimum_scaling_step_size = var.minimum_scaling_step_size
      maximum_scaling_step_size = var.maximum_scaling_step_size
      status                    = var.managed_scaling_status
      target_capacity           = var.managed_scaling_target_capacity
    }
  }

  depends_on = [module.ecs_asg.this_autoscaling_group_id]
}

module "ecs_cluster" {
  source  = "HENNGE/ecs/aws"
  version = "1.0.0"

  name = var.cluster_name

  settings = var.cluster_settings

  capacity_providers = [aws_ecs_capacity_provider.ecs_cap_prov.name]

  default_capacity_provider_strategy = [{
    capacity_provider = aws_ecs_capacity_provider.ecs_cap_prov.name
    weight            = var.default_capacity_provider_weight
    base              = var.default_capacity_provider_base
  }]

  tags = merge(var.tags, map("Environment", format("%s", var.environment)))
}

module "ecs_ec2_profile" {
  source = "github.com/terraform-aws-modules/terraform-aws-ecs//modules/ecs-instance-profile/?ref=v2.0.0"
  name   = var.cluster_name
}

module "ecs_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "${var.cluster_name}-ecs"

  # Launch configuration

  # Using the same name with asg to prevent capacity provider creating issue
  lc_name = "${var.cluster_name}-lc"

  image_id             = data.aws_ami.amazon_linux_ecs.id
  instance_type        = var.ec2_instance_type
  spot_price           = var.spot_price
  security_groups      = compact(concat([module.ecs_security_group.this_security_group_id], var.vpc_security_group_ids))
  iam_instance_profile = module.ecs_ec2_profile.this_iam_instance_profile_id
  user_data            = data.template_file.user_data.rendered
  key_name             = var.ssh_key_pair_name

  ebs_block_device = [
    {
      device_name           = "/dev/xvdcz"
      volume_type           = "gp2"
      volume_size           = var.ec2_data_volume_size_gb
      delete_on_termination = true
    },
  ]

  # Auto scaling group
  asg_name = "${var.cluster_name}-asg-${random_id.cap_prov.hex}"
  //asg_name                  = "${var.cluster_name}-asg"
  vpc_zone_identifier       = var.subnet_ids
  health_check_type         = "EC2"
  min_size                  = var.asg_min_size
  max_size                  = var.asg_max_size
  desired_capacity          = var.asg_desired_capacity
  wait_for_capacity_timeout = var.asg_wait_for_capacity_timeout
  protect_from_scale_in     = (var.managed_termination_protection == "ENABLED" ? true : false)

  # TODO: add suffix
  tags_as_map = merge(var.tags, map("Environment", format("%s", var.environment)))

}

data "template_file" "user_data" {
  template = file("${path.module}/templates/user-data.sh")

  vars = {
    preinstall_user_script  = var.preinstall_user_script
    postinstall_user_script = var.postinstall_user_script
    ecs_cluster             = var.cluster_name
    efs_servername          = var.efs_servername
    efs_mountpoint          = var.efs_mountpoint
    ecs_secrets_support     = var.ecs_secrets_support
  }
}

module "ecs_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  create = (var.alb_security_group_id != null ? true : false)

  name   = "${var.cluster_name}-ecs-sg"
  vpc_id = var.vpc_id


  # Ingress from ALB only
  number_of_computed_ingress_with_source_security_group_id = 1

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "all-all"
      source_security_group_id = var.alb_security_group_id
    }
  ]

  # Allow all egress
  egress_cidr_blocks      = ["0.0.0.0/0"]
  egress_ipv6_cidr_blocks = ["::/0"]
  egress_rules            = ["all-all"]

  tags = var.tags
}
