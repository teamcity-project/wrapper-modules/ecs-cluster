output "ecs_cluster_arn" {
  value       = module.ecs_cluster.arn
  description = "ECS cluster ARN"
}

output "ecs_cluster_name" {
  value       = module.ecs_cluster.name
  description = "ECS cluster name"
}

output "ecs_capacity_provider_name" {
  value       = aws_ecs_capacity_provider.ecs_cap_prov.name
  description = "ECS cluster default capacity provider"
}
