# ECS-cluster wrapper module for Teamcity project

This module is wrapper for these modules:
* [terraform-aws-security-group](https://github.com/terraform-aws-modules/terraform-aws-security-group)
* [terraform-aws-autoscaling](https://github.com/terraform-aws-modules/terraform-aws-autoscaling)
* [terraform-aws-ecs](github.com/terraform-aws-modules/terraform-aws-ecs)
* [terraform-aws-ecs by HENNGE](https://github.com/HENNGE/terraform-aws-ecs) (and use it as solution example)

The module creates:
  - ECS Capacity Provider attached to ASG
  - EC2 profile with IAM role for access to EC2 and CloudWatch
  - Security group for access from ALB security group
  - Launch configuration and ASG with automounting EFS resources at EC2 start (optional)

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.42 |
| random | n/a |
| template | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| alb\_security\_group\_id | ALB security group ID attached to ECS EC2 security group | `string` | n/a | yes |
| asg\_desired\_capacity | Desired number of ECS EC2 instances in auto scale group | `number` | n/a | yes |
| asg\_max\_size | The maximum size of the auto scale group of ECS EC2 instances | `number` | `10` | no |
| asg\_min\_size | The minimum size of the auto scale group of ECS EC2 instances | `number` | `0` | no |
| asg\_wait\_for\_capacity\_timeout | A maximum duration that Terraform should wait for ASG instances to be healthy before timing out | `number` | `0` | no |
| cluster\_name | ECS cluster name | `string` | n/a | yes |
| cluster\_settings | ECS cluster settings (CloudWatch Container Insights, for example) | `list(map(string))` | `[]` | no |
| default\_capacity\_provider\_base | The number of tasks, at a minimum, to run on the specified capacity provider | `number` | `0` | no |
| default\_capacity\_provider\_weight | The relative percentage of the total number of launched tasks that should use the default capacity provider | `number` | `1` | no |
| ec2\_data\_volume\_size\_gb | EBS data volume size | `number` | `22` | no |
| ec2\_instance\_type | ECS EC2 instance type | `string` | n/a | yes |
| ecs\_ami\_filter | Filter for AWS ECS AMI | `list(string)` | <pre>[<br>  "amzn-ami-*-amazon-ecs-optimized"<br>]</pre> | no |
| ecs\_secrets\_support | Enable support of access to ECS secrets from SSM or Secret Manager | `bool` | `false` | no |
| efs\_mountpoint | Mountpoint for EFS on ECS EC2 | `string` | `"/mnt/efs"` | no |
| efs\_servername | EFS server hostname | `string` | `""` | no |
| environment | Environment name | `string` | `""` | no |
| managed\_scaling\_status | Whether auto scaling is managed by ECS. Valid values are ENABLED and DISABLED | `string` | `"ENABLED"` | no |
| managed\_scaling\_target\_capacity | The target utilization for the capacity provider. A number between 1 and 100 | `number` | `100` | no |
| managed\_termination\_protection | Enables or disables container-aware termination of instances in the auto scaling group when scale-in happens | `string` | `"ENABLED"` | no |
| maximum\_scaling\_step\_size | The maximum step adjustment size. A number between 1 and 10,000 | `number` | `1000` | no |
| minimum\_scaling\_step\_size | The minimum step adjustment size. A number between 1 and 10,000 | `number` | `1` | no |
| postinstall\_user\_script | The command executed at the end of startup user script. For using with any custom purposes | `string` | `""` | no |
| preinstall\_user\_script | The command executed at the beginning of startup user script. For using with any custom purposes | `string` | `""` | no |
| region | AWS region | `string` | n/a | yes |
| spot\_price | The price to use for reserving spot instances | `string` | `""` | no |
| ssh\_key\_pair\_name | The SSH key name attached to ECS EC2 | `string` | `""` | no |
| subnet\_ids | A list of VPC subnet IDs for ECS EC2 | `list(string)` | n/a | yes |
| suffix | Suffix in ECS cluster name | `string` | `""` | no |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |
| vpc\_id | The VPC for security group creating | `string` | n/a | yes |
| vpc\_security\_group\_ids | List of VPC security groups to associate with ECS EC2 | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| ecs\_capacity\_provider\_name | ECS cluster default capacity provider |
| ecs\_cluster\_arn | ECS cluster ARN |
| ecs\_cluster\_name | ECS cluster name |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Known issues
There is no possibility to delete or update existing Capacity Provider for ECS at this moment (with AWS provider v2.48.0)
(see [issue 11287](https://github.com/terraform-providers/terraform-provider-aws/issues/11287) and
[issue 633](https://github.com/aws/containers-roadmap/issues/633)), so Terraform creates this one with random name for every time.
